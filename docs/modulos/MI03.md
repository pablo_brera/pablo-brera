---
hide:
    - toc
---

# MI 03 - Innovación Abierta

**¿QUE ES LA INNOVACIÓN ABIERTA?**

Consiste en una estrategia para hacer que la innovación no se limite únicamente a la parte interna de una organización o a su departamento de I+D, sino que se combine con el conocimiento externo.


![](../images/MI03/h.jpg){width="300"}

Henry Chesbrough está considerado a nivel mundial como el padre de la innovación abierta. Es profesor en la Haas School of Business de la University of California (Berkeley) y director ejecutivo del Center for Open Innovation, que se dedica a la investigación y la docencia relacionadas con el impacto del conocimiento repartido mundialmente sobre crecimiento, estrategia e innovación.

**10 puntos clave en un proceso de implantación de un modelo de innovación abierta:**

***Compromiso de la dirección*** Sin duda es fundamental. La dirección ha de transmitir la importancia que la innovación abierta tiene para la empresa y va a tener en el futuro.

***Participación de la dirección*** No sólo hay que transmitir la importancia del proyecto, sino que es importante la participación. Los cambios que se pueden generar en las empresas son críticos, y conviene que la dirección participe de esos cambios y tenga información de ellos de primera mano.

***La información es clave*** Una parte importante de la implantación de un modelo de IA es la gestión de la información. Hay que cambiar la forma en la que se gestiona y huir de modelos tradicionales que utilizan la información como una posesión.

***El objetivo es el conocimiento*** La información se ha de gestionar para conseguir conocimiento, para mejorar el que posea la organización y para aumentarlo.

***Tan importante como la organización son los trabajadores*** El objetivo de un programa de IA no ha de ser sólo aportar valor a la organización, sino también a los trabajadores. Ellos han de estar convencidos de las bondades de este modelo, del beneficio que van a obtener con él. Al fin y al cabo son los que han de gestionarlo.

***Gestionar la responsabilidad que implica la libertad*** La implantación de un modelo de IA suele implicar un aumento de libertad de decisión y gestión por parte de los trabajadores, pero eso implica que ellos también han de ser responsables de sus decisiones. Y responsabilidad no implica castigo, sino gestión del error.

***El error forma parte de la gestión, está en el ADN*** La organización ha de ser capaz de estudiarlo, aprender de él y mejorar así la organización. Es fundamental que no lo ocultemos ni lo castiguemos.

***Redarquía, la red es un concepto clave*** Tanto de manera externa como interna, es fundamental desarrollar redes que fortalezcan nuestra capacidad de generar y transmitir conocimiento.

***Compartir información es bueno*** A nivel interno mejora el conocimiento de la organización y a nivel externo aumenta las posibilidades de relevancia y encontrar Partners para proyectos. Además nos permite obtener otras visiones e interpretaciones de la información compartida enriqueciendo y aumentando las posibilidades de éxito.

***La Innovación Abierta no es una metodología*** No estamos hablando de un proceso pautado que se implanta de igual manera en todas las empresas. Se trata de un enfoque, un modelo abierto de gestión que se ha de adaptar a las necesidades y características de cada empresa.

**REGLAS PARA LA INNOVACIÓN ABIERTA**

***El problema o la pregunta que nos ocupa***
Antes de que comunidades más amplias de pensadores puedan aportar sus mejores ideas, necesitan saber a qué se enfrentan. Debe brindar información clara y precisa sobre el problema que está tratando de resolver.

***Los incentivos disponibles***
A la gente le gusta ser recompensada por sus ideas. Por lo tanto, para obtener las sugerencias más transformadoras, debe ofrecer algo a cambio. Esto puede tomar la forma de una recompensa económica, reconocimiento profesional o publicidad valiosa.

***Expectativas de propiedad intelectual***
Debe establecer quién será el propietario de la propiedad intelectual si su empresa decide adoptar una innovación como resultado de la innovación abierta. Esto es fundamental para que los participantes tengan confianza en el proceso.

***Cómo y cuándo las personas deben enviar ideas***
Si les pide a las personas que envíen sus mejores ideas, deben conocer el formato deseado para hacerlo, incluidos los plazos.

**MODELOS DE INNOVACIÓN ABIERTA**

***Desafíos***
Las empresas pueden establecer desafíos de innovación para recopilar ideas y encontrar soluciones. Estos pueden ser eventos públicos en los que cualquiera puede contribuir, o pueden ser privados con socios específicamente seleccionados.

***Asociaciones de empresas emergentes***
Las asociaciones entre startups y empresas corporativas son otra forma de trabajar juntos para encontrar una solución a un problema identificado. Estas asociaciones son una excelente manera de aprovechar las fortalezas de cada empresa y ofrecer beneficios mutuos.

***Incubadora / aceleradora de inicio***
Una incubadora o aceleradora de startups es similar a una asociación, pero también implica que el negocio corporativo invierta capital en la startup. A menudo, la empresa tendrá un equipo interno trabajando en el proyecto, con la puesta en marcha allí para ayudarlos con conocimientos y experiencia específicos.

***Hackatones***
Los hackatones son similares a los desafíos de innovación, pero también profundizan en los detalles del proyecto, a menudo hasta la fase mínima del producto viable. El objetivo de los hackatones es trabajar lo más rápido posible, canalizando la experiencia y el conocimiento creativos para generar impulso en un período de tiempo muy corto.

***laboratorios de co-creación***
Los laboratorios de co-creación pueden ser internos de una empresa o convocados externamente junto con empleados, clientes y otros socios. Los laboratorios de cocreación son lugares dedicados a la innovación, con los recursos, la tutoría y el conocimiento que las personas necesitan para explorar preguntas desafiantes.

***OPEN SOURCE***
Open source es una expresión de la lengua inglesa que puede traducirse como “fuente abierta”. Se califica como open source, a los programas informáticos que permiten el acceso a su código de programación, lo que facilita modificaciones por parte de otros programadores ajenos a los creadores originales del software en cuestión.

***¿Cuáles son las principales diferencias del software libre frente a open source?***
La principal es que el código abierto es menos estricto que el software libre, por lo que en la práctica todo software libre se puede calificar como código abierto, aunque no todo el software de código abierto tiene por qué ser libre.

El Software Libre tiene diferencias de índole filosóficas con el Open Source, mientras que desde la FSF (Free Software Foundation) siempre se priorizan los aspectos de índole ética, en el Open Source se destacan las aspectos técnicos sobre cualquier discusión moral acerca de las licencias y derechos.

***OPEN HARDWARE***
El término open hardware, u open source hardware, se refiere al hardware cuyo diseño se hace públicamente disponible para que cualquiera pueda estudiarlo, modificarlo y distribuirlo, además de poder producir y vender hardware basado en ese diseño.

![](../images/MI03/1.jpg)

***El paradigma actual: Un sistema de innovación cerrada***




![](../images/MI03/2.jpg)

***Paradigma de innovación abierta***
