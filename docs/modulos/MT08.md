---
hide:
    - toc
---

# MT 08 - Diseño y producción de circuitos - Milling CNC de PCB

**Introducción**

¿Qué es un PCB? Una tarjeta de circuito impreso es un circuito cuyos componentes y conductores están contenidos dentro de una estructura mecánica. Las funciones conductoras incluyen trazas de cobre (o pistas), terminales, disipadores de calor o conductores planos. La estructura mecánica se hace con material laminado aislante entre capas de material conductivo. La estructura general es chapada y está cubierta con una máscara de soldadura no conductora y una pantalla de impresión para la ubicación de leyenda de componentes electrónicos.

![](../images/MT08/pcb.png)

La tarjeta de circuito impreso está construida por capas que se alternan de cobre conductor con capas de material aislante no conductor. Durante la fabricación, se graban las capas de cobre internas dejando trazas de cobre intencionadas para conectar los componentes de circuito. Una vez laminado el material de aislamiento es grabado a las capas de cobre y así sucesivamente hasta que la tarjeta de circuito impreso esté completa

Los componentes se agregan a las capas externas de la tarjeta de circuito impreso cuando todas las capas se han grabado y laminado juntas. Las partes de montaje superficial (SMD) se aplican automáticamente con robots y las partes con orificio pasante (Through Hole) se colocan manualmente. Luego, todas las partes se sueldan en la tarjeta utilizando técnicas tales como reflujo o soldadura por ola.

![](../images/MT08/nopcb.jpg)

Antes de las tarjetas de circuito impreso, los circuitos eléctricos se construían uniendo los cables individuales a los componentes. Los caminos conductores se lograron al soldar componentes metálicos junto con cable. Los grandes circuitos con muchos componentes contienen muchos cables. La cantidad de cables era tan grande que podían enredarse u ocupar un gran espacio dentro de un diseño. La fabricación era lenta y necesitaba soldadura manual de componentes múltiples para sus conexiones por cable.

El diseño de PCB se hace utilizando softwares especificos como Kicad, Eagle o Altium.

Kicad es la única ópcion dentro de la filosofía Open Source. Por eso es ideal para comenzar a incursionar en el mundo del diseño de PCBs.

<a href="https://www.kicad.org/">Web de Kicad</a>

La fabricación de PCBs se puede realizar de varias maneras. desde 100% artesanales como el dibujo de las pistas con marcadores y desgaste de la plancha de cobre con ácido hasta la fabricación industrial. El punto medios seria el desbaste CNC de una plancha de cobre llamado Milling.


**Desafío**

El desafío de este módulo consistia en el diseño en Kicad de un circuito impreso y su posterior fabricación utilizando el proceso de CNC Milling utilizando la fresadora CNC Mipec de UTEC.


**Propuesta**

![](../images/MT08/pcb1.jpg){width="800"}

Para este desafío realicé el diseño de la placa de control de la ducha del proyecto final. Viajé a Fray Bentos y pude realizar el milleado de la placa. Luego la solde y comprobé su funcinamiento.

![](../images/MT08/mipec.PNG){width="800"}

Los componentes utilizados son todos through hole debido a que son el tipo de componentes que se consiguen en las tiendas de electronica de nuestro pais.

![](../images/MT08/pcb2.jpg){width="800"}

La placa contenia relés para controlar las valvulas y bombas del proyecto, transistores para activar estos relés y la Arduino Nano encargada de controlar los automatismos de la ducha.

El archivo fuente de Kicad se puede descargar <a href="https://drive.google.com/file/d/15l3VoT4WZWNrnUqhiuRo2NwbiBQcJd9I/view?usp=sharing">aca</a>
