---
hide:
    - toc
---

# MT 04 - Modelado 3D

**Introducción**

En este modulo trabajamos sobre los conceptos de modelado 3D parametrico y nos basamos en el software Fusion 360.

El principal factor teórico fue la diferencia entre el modelado por Mesh y el modelado por Nurbs.

La principal diferencia entre el modelado Mesh y Nurbs es la forma en que la computadora calcula la malla.

![](../images/MT04/vs.png)

Para el modelado de Mesh, calcula polígonos, que son planos que comprenden una forma 3D (la forma en que un cubo está hecho de 6 cuadrados, por ejemplo).

Nurbs (que es la abreviatura de B-splines racionales no uniformes) utiliza matemáticas complejas para calcular la superficie de un modelo. Esto significa que es mucho más adecuado para modelos donde se necesita un alto grado de precisión.

Un buen ejemplo en el que esto es importante es en ingeniería. Cuando un ingeniero está tratando de calcular la aerodinámica de un avión o automóvil y quiere realizar pruebas en un túnel de viento digital.

La desventaja de esta forma altamente matemática de calcular el tamaño de un modelo es que se vuelve más difícil de calcular para la computadora, lo que significa que no es adecuada para aplicaciones donde los tiempos de renderización deben ser rápidos. Por ejemplo, nunca encontrarás superficies Nurbs en los videojuegos.

En el modelado Mesh es imposible hacer una curva perfectamente suave, debido a que siempre se calculan como una línea recta entre puntos.

Sin embargo, al usar grupos de suavizado y una gran cantidad de polígonos, puede crear secciones que se perciben como una curva suave en la pantalla.

Nurbs tiene la ventaja adicional de tener un tamaño de archivo más pequeño, ya que todos los datos contenidos son puntos matemáticos. Esto también significa que los datos son fáciles de leer y comprender entre diferentes programas, por lo que nunca terminará con mallas dañadas, como puede suceder cuando se transfieren modelos poligonales.

Simplificando mucho Mesh es asimilable a una imagen raster mientras que nurbs es asimilable a una imagén vectorial.


**Desafío**

El desafío de este modulo era diseñar y modelar en 3D un objeto utilizando el software Fusion 360.

**Propuesta**

Mi inicio con Fusion 360 no fue muy sencillo ya que cuento cn experiencia previa en Rhinoceros y las lógicas, intefases y dinámicas de ambos programas son completamente distintas.

Para lograr incorporar las logicas realice un modelado siguiendo un tutorial que explicaba al detalle como diseñar un bloque de Lego.

Si bien la pieza tiene puentes, su distancia es lo suficientemente corta como para poder ser impresa en 3D sin necesidad de utilizar soportes.

![](../images/MT04/lego1.png)
![](../images/MT04/lego2.png)
