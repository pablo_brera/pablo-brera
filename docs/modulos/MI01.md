---
hide:
    - toc
---

# MI 01 - Emprendimientos Dinámicos

**Introducción**

En el workshop de emprendimientos fue intensivo y cubrio varios aspectos y herraientas metodológicas que deben ser tenidos en cuenta al momento de desarrollar una idea de negocio.

Entre ellas se desarrollaron las herramientas de:

Bussiness Model Canvas

Diagrama de Gantt

Design Thinking

Human Centered Design

UX Research

Design Sprint de Google

El concepto de "Propósito"

Performance Marketing

Brand Marketing

Key Performance Indicators (KPIs)

En el grupo trabajamos con un proyecto de plataforma de pago instantanea a traves de una app celular conectada a las cuentas bancarias de los usuarios.


**Desafío**

El desafío de este modulo consistia en realizar un bussiness model canvas del aspecto comercial del proyecto.

También se debía realizar una planificación de la ejecución del proyecto utilizando la herramienta diagrama de Gantt.

**Propuesta**

Realicé el Bussiness Model Canvas de mi emprendimiento actual para aprovechar al máximo los conocimientos del curso ya que aún no tengo definido el proyecto final.

Bussiness model canvas:

![](../images/MI01/bmc.jpg)

Diagrama de Gantt:

![](../images/MI01/gantt.png)
