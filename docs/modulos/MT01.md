---
hide:
    - toc
---

# MT 01 - Introducción al diseño web - Herramientas digitales

**Introducción**

En este modulo comenzamos aprendiendo la historia de internet y como es su funcionamiento para luego conocer herramientas que nos permitiran diseñar nuestra propia pagina web, subirla a un hosting, publicarla y poderla ir actualizando.

Para llevar a cabo el control de los cambios de la web y poderlos revertir, revisar, etc usaremos a Gitlab que es un servicio de control de versiones de archivos muy util tanto para desarrollo de webs, como control de cambios en la programacion de software, etc.

![](../images/MT01/git.png)

Herramientas que usaremos para crear la web:

GitLab: servicio web de control de versiones y desarrollo de software colaborativo basado en Git y open source.

Markdown: lenguaje utilizado para escribir documentos con texto normal y que sen encarga de traducirlo al formato HTML de la página web de manera sencilla.

MkDocs: es un generador de sitios web estáticos que contruye la pagina utilizando archivos escritos en lenguaje Markdown.

Atom: es un editor de texto que trabaja en en conjunto con Markdown y con GitLab.

**Desafío**

El desafío de este modulo consisita en realizar la web que estas viendo en este momento :)

**Propuesta**

Esta web aun se encuentra en desarrollo, tanto porque el curso aún continua y se van a ir agregando mas modulos, como porque voy adquiriendo maor conocimiento de Markdown y realizando mejoras de diferente tipo a la web.
