---
hide:
    - toc
---

# TI 01 - Taller Integrador

**Introducción**

El taller constaba de 3 objetivos principales:

Adaptar la impresora Anet A8 para poder imprimir utilizando materiales cerámicos y/o órganicos
Imprimir piezas en estos materiales y estudiar su comportamiento
Soldar el circuito PCB diseñado en el módulo MT08 - "Diseño y producción de circuitos - Milling CNC de PCB"


**Resultados**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSQh8tZijAkBdCk13Mxp2nD1SBPvqjvF6vG_RyUdsHsvWAF2OwT40rxzilp0JgB-kOEbd34jScc8Clh/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


**Video institucional del taller**

<iframe width="560" height="315" src="https://www.youtube.com/embed/F8Dq9xU6Ftc" title="Workshop 2022 | Fabricación Digital e Innovación" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
