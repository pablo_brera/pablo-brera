---
hide:
    - toc
---

# Planificación

**Esquema del sistema**

Para comenzar el desarrollo del prototipo de manera organizada, realicé un esquema del sistema y de todos los componentes necesarios. El sistema se compone de dos circuitos de agua, uno encargado de calentar el agua del deposito de agua limpia y otro encargado de recoger el agua del depósito de aguas grises, filtrarla y llevarla hasta el deposito de agua limpia. Ambos sistemas son coordinados y controlados por un panel de control en donde el usuario puede encender el sistema y seleccionar la temperatura objetivo del agua para bañarse. Todos los elementos de los circuitos de agua son controlados por este panel de control de manera automatizada.

![](../images/Esquema.png)

**Compra de componentes**

Debido a la falta de muchos componentes en Uruguay, varios de ellos debieron de ser adquiridos en Estados Unidos e importados a Uruguay.

Las tiendas de Estados Unidos utilizadas para buscar componentes fueron:
www.digikey.com
www.mouser.com
www.amazon.com
www.ebay.com
www.adafruit.com
www.sparkfun.com
www.dfrobot.com

Las tiendas Uruguayas utilizadas fueron:
www.eneka.com
www.rubenaprahamian.com
www.robotec.com.uy
www.mercadolibre.com.uy
La ferreteria del barrio :)

**Componentes**

**Bomba de agua**
![](../images/bomba.jpg){width="300"}

Seleccione esta bomba por 3 caracteristicas principalmente:

1 - Que funciona con 12 volts que es el voltaje mas sencillo de adquirir del circuito eléctrico de la camper.

2 - Que puede chupar el agua del tanque aunque le haya ingresado aire al sistema de mangueras (cosa que puede suceder muy seguido)

3 - Que su caudal es suficiente para encender el calefon a gas y obtener su máximo rendimiento.

<a href="https://www.amazon.com/gp/css/order-history?ref_=nav_AccountFlyout_orders">Link a componente en Amazon</a>

**Electrovalvulas**
![](../images/electrovalvula.jpg){width="300"}

Seleccione estas electrovalvulas por 4 razones:

1 - Bajo costo

2 - Funcionan con 12 volts

3 - Cuando no estan energizadas se mantienen cerradas (tipo normal-cerrado). Esto ahorra energia ya que cuando el sistema esta apagado, todas las electrovalvulas quedan cerradas.

4 - Tienen el tipo de conexion que sirve para las mangueras utilizadas en las instalaciones de agua de las campers.

<a href="https://www.amazon.com/dp/B089RC5BBK?psc=1&ref=ppx_yo2ov_dt_b_product_details">Link a componente en Amazon</a>

**Sensor de flujo de agua**
![](../images/flujo.jpg){width="300"}

Seleccione este sensor debido a que:

1 -  es totalmente compatible con Arduino

2 - Tiene el tipo de conexion de las mangueras


<a href="https://www.amazon.com/dp/B07RF57QF8?ref=ppx_yo2ov_dt_b_product_details&th=1">Link a componente en Amazon</a>

**Display de segmetos**
![](../images/display.jpeg){width="300"}

Seleccione este display debido a que:

1 - Es totalmente compatible con Arduino y utiliza pocos pines para recibir la información

2 - Tiene la posibilidad de mostrar dos valores en simultaneo lo cual es ideal para mostrar la temperatura actual del agua y la temperatura objetivo.

3 - Tiene luminosidad suficiente para atravesar una impresion 3D de bajo espesor y de esta manera puede mostrar los digitos y ser cubierto del agua por la carcaza impresa.


<a href="https://www.robotec.com.uy/productos/TX336">Link a componente en Robotec</a>

**Sensor de temperatura**
![](../images/temp.jfif){width="300"}

Seleccione este sensor debido a que:

1 - Es totalmente sumergible en agua caliente, aguantando hasta 100 grados de temperatura.

2 - Tiene buena sensibilidad y tiempo de respuesta.

3 - Es un sensor muy conocido y existe mucha documentación de su utilización.

4 - Estaba disponible en la biblioteca de materiales de UTEC.


<a href="https://www.dfrobot.com/product-689.html">Link a componente en DFRobot</a>

**Arduino Nano**
![](../images/nano.jpg){width="300"}

Seleccione este modelo de Arduino debido a que:

1 - Es mucho mas pequeña que el resto de modelos de Arduino pero tiene acceso a suficientes pines como para lo que necesitaba controlar.

2 - Utiliza 5 volts por lo que se puede comunicar con los sensores seleccionados (a diferencia de otras placas que utilizan 3,3 volts).

4 - Estaba disponible en la biblioteca de materiales de UTEC.


<a href="https://store-usa.arduino.cc/products/arduino-nano?selectedStore=us">Link a componente en Arduino</a>

**Otros componentes**

Componentes mas pequeños y estandar fueron utilizados para la placa electrónica y para el sistema de agua. Entre ellos, réles, abrazaderas, mangueras de alta presion, cables, etc.
