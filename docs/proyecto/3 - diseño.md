---
hide:
    - toc
---

# Diseño


El principal componente del sistema es el tablero de control. El tablero es el encargado de controlar todos los componentes electronicos del sistema y a su vez es la interfaz con el usuario. El tablero esta compuesto por un panel de usuario y un circuito de control.

**Panel de usuario**

El panel de usuario fue diseñado en Rhinoceros e impreso con impresora FDM utilizando PLA de 1.75mm blanco y negro.

![](../images/Panel.png){width="600"}

El panel de usuario es la unica interfaz del sistema con el usuario de la ducha. En el se encuentran los botones que permiten encender y apagar el sistema y seleccionar la temperatura del agua de la ducha. Tambien se encuentra un display que muestra la temperatura actual del agua y la temperatura objetivo. Debajo del display hay un LED que indica si el sistema se encuentra calentando el agua para alcanzar la temperatura seleccionada por el usuario.

![](../images/Panel2.png){width="600"}

El panel tambien posee cuatro agujeros que permiten adosarlo a la pared de la camioneta a traves de tornillos. En su lado trasero tiene: una contraforma para sostener la placa electronica que controla todos los componentes de la ducha, una contraforma para sostener el display y una contraforma para sostener el LED que indica el ciclo de calentamiento.

El panel esta pensado para ser utilizado al momento de ducharse por lo que fue pensado como una sola pieza para que no lo afecte el agua. Tambien sus inscripciones fueron diseñadas para ser impresas en el propio panel y que el mismo pueda ser limpiado y utilizado con las manos mojadas sin correr riesgo de que se borren las letras.

![](../images/panel3.jpg){width="800"}


*Primera impresión del panel*

**Botones del panel**

![](../images/botones.jpg){width="800"}

Los botones fueron uno de los componentes del panel donde intenté innovar en el diseño a fin de lograr que fueran resistentes a las salpicaduras. El objetivo era que se pudieran utilizar con las manos mojadas y que funcionaran sin presentar ningun inconveniente.

La primera versión de los botones (a la izquierda) consistia en segmentos impresos en 3D utilizando filamento PLA conductivo. La intención es que cuand el usuario apriete el "boton", su dedo haga de puente entre los segmentos que conforman el icono del boton y cierre el circuito. Sin embargo al probar esta idea con el multimetro, descubri que la conductividad de la piel del dedo sumada a la resistencia del PLA, generaba mucha resistencia y el Arduino no iba a percibir la diferencia entre el botón presionado y sin presionar.

La segunda versión de los botones (al medio) consistia en un botón flexible que al momento de ser presionado hacia tocar a dos trozos del filamento conductivo para realizar el puente y enviar la señal al Arduino. Sin embargo me econtré con que el filamento conductivo es mucho menos conductivo en su estado original que luego de ser impreso. De esta manera, se utilizaba el filamento en su estado original y la corriente conducida tampoco alcanzaba a ser percibida por el Arduino.

La tercer versión (a la derecha) tambien utiliza un botón flexible impreso en el propio panel pero en lugar de hacer que se toquen dos trozos de filamento, lo que se tocas son dos trozos de alambre. Esta versión funciono a la perfección y fue la utilizada en la versión final del panel.

La distancia entre los cables y la disposición dentro del botón, impiden que cualquier entrada de agua pueda unir los cables y generar falsos "aprietes" del botón.

**Circuito de control**

El circuito de control fue diseñado en Kicad y fabricado por el metodo de fresado utilizando la fresadora Mipec 4MILL300. Se diseño con componentes through hole para facilitar el proceso de soldado y porque son mas faciles de conseguir en Uruguay.

![](../images/shield.png){width="800"}

La fabricación de esta placa PCB se realizó en el LabA de UTEC Fray Bentos por lo que solamente existe una versión de la misma.

Sin embargo en el proceso soldado de los componentes y de construcción de la ducha surgieron varios inconvenientes a mejorar en futuras versiones.

El primer inconveniente fue que por alguna razon el software del CNC no lograba interpretar el plano de tierra y por ende todos los contactos a tierra quedaron como "islas" y hubo que puentearlos y soldarlos al plano de tierra. Si bien junto con Mateo buscamos varias formas de solucionarlo, no logramos entender de donde surgia el error de interpretación.

![](../images/shield2.jpg){width="800"}

El segundo inconveniente fue que no tuve en cuenta la distancia del conector USB de programación al posicionar los agujeros de los pines para soldar la Arduino Nano. Esto hace que el cable de programación no logré insertarse adecuadamente y fue necesario forzar el cable para poder colocarlo.

![](../images/shield3.jpg){width="800"}

Mas allá de ests inconvenientes, el circuito de control funcionó a la perfección. Primero probé todos los componentes de manera individual para asegurarme de su correcto funcionamiento antes de armar el prototipo final.

![](../images/shield4.jpg){width="800"}

**Filtro de agua**

El filtrado de las aguas grises resulta esencial para el exito de este proyecto. Para nuestra suerte, existe un proyecto de filtro de aguas de ducha muy bien documentado, con ensayos clinicos del nivel de filtrado del agua y totalmente open source. Por lo tanto en este proyecto decidí dejar el filtro fuera del alcance y al momento de implementar el prototipo en la camper real, usaré el filtro de dicho proyecto.

El proyecto se llama Showerloop y se puede conocer <a href="https://showerloop.org/page-4-components">aca</a>

**Otras piezas**

Al no contar con una camioneta camper para poder instalar el prototipo, creé una simulacion de circuito de agua en un panel de madera. El circuito tiene todos los componentes que tendria la camioneta camper a excepcion del calefón a gas. El proceso de calentamiento de agua del calefón a gas se simuló llenando un tanque con agua caliente y atravesando la manguera por él para que el agua se calentara de igual forma que lo haria en el calefon. Para poder hacer funcionar esta maqueta diseñé varias piezas para sostener las partes, recoger el agua, etc. Sin embargo no las considero parte del proyecto por ser piezas que no tendrian utilidad en la aplicación real del sistema. Igualmente los archivos de diseño de estas piezas se encuentran en la seccion de archivos por si resultan de utilidad para algun fin.

![](../images/maqueta.jpg){width="800"}
