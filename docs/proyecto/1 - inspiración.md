---
hide:
    - toc
---

# Inspiración

**El sueño**

Durante el confinamiento de la pandemia me dedique a ver videos en YouTube de temas variados. Uno de los temas en los que más incursioné fue en el de las camionetas tipo furgón adaptadas para funcionar como mini casas. La denominación de estos vehículos es "Camper" y se diferencian de los motorhomes y de las casas rodantes en el hecho de que su estructura exterior mantiene el furgón de la camioneta original. Este tipo de vehículos me atraen desde el punto de vista del diseño y desde el punto de vista del estilo de vida minimalista donde por las restricciones de espacio solo podes llevar lo realmente necesario. Me propuse la idea de algún día poder construir una Camper y utilizarla para recorrer Europa durante algunos meses. Si bien por ahora es un sueño un poco delirante, tal vez algún día lo consiga. :)

![](../images/camper.jpg)

**El proyecto**

El proyecto final del EFDIA surge inspirado en uno de los principales problemas de la vida en una Camper: la ducha!

Debido a las restricciones de espacio los tanques de agua de las Camper son muy pequeños y no contienen suficiente agua como para poder darse una ducha continua. Debido a esto muchos viajeros deciden directamente prescindir de la ducha y rebuscarse por para ducharse en gasolineras, casas de conocidos, etc. Los viajeros que instalan duchas se encuentran con tres problemas:

1 - La poca agua disponible solo les permite usar la ducha por intervalos muy cortos

2 - El agua resultante de la ducha rellena rápidamente el depósito de aguas residuales (grises) por lo que deben buscar un lugar apto para vaciarlo

3 - Debido a la limitante de espacio y de electricidad utilizan para calentar el agua calentadores a gas los cuales si bien son instantáneos, no tan instantáneas como para lograr una temperatura constante si uno abre y cierra rápidamente la canilla de la ducha.

El proyecto busca generar una ducha capaz de reciclar el agua de la ducha para evitar los problemas de abastecimiento y deposición de las aguas grises. Esto permitiría la utilización de la ducha de una manera mucho más libre lo cual es sumamente necesario para la comodidad en viajes largos. También busca solucionar el problema de la temperatura constante del agua generando un sistema que precaliente el agua a una temperatura seleccionada utilizando el calefón a gas de manera independiente del flujo de agua generado por la utilización de la ducha.
