---
hide:
    - toc
---

# Resultado

**Lámina de presentación**
![](../images/Slide.jpg)

**Video mostrando el funcionamiento**
<iframe src="https://player.vimeo.com/video/733098165?h=466f15a195&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="800" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="UTEC - EFDIA - Proyecto Final Pablo Brera"></iframe>
**Archivos del proyecto**

La electrónica fue diseñada utilizando Kicad.
El archivo fuente lo podes descargar de <a href="https://drive.google.com/file/d/15l3VoT4WZWNrnUqhiuRo2NwbiBQcJd9I/view?usp=sharing">aca</a>

El panel fue diseñado utilizando Rhinoceros.
El archivo fuente lo podes descargar de <a href="https://drive.google.com/file/d/15i-GKZWSjUREtRoYNdxQihBsClQhYsxk/view?usp=sharing">aca</a>

Las piezas extra utilizadas para el ensamblado de la maqueta fueron diseñadas en Rhinoceros.
El archivo fuente lo podes descargar de <a href="https://drive.google.com/file/d/15jHki96mFiRIxgceBz-YpVBgwymMUU9F/view?usp=sharing">aca</a>

La programación del circuito de control fue realizada en Arduino.
El archivo fuente lo podes descargar de <a href="https://drive.google.com/file/d/15kwHuOrYYWEQS5V6c8pm0O1LNsoA9hHJ/view?usp=sharing">aca</a>
