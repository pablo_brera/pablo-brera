# Sobre mi

![](../images/Equipo.jpg)

Mi nombre es Pablo (Soy el de remera blanca), estudié diseño industrial y desde chico me gusta desarmar todas las cosas para entender cómo funcionan.
Cuenta la leyenda que una de las primeras palabras que dije fue "chufa" (de enchufa) y que perseguía a mi madre que pidiéndole enchufara todo lo que encontraba para ver que sucedía.

En 2018 comencé con dos amigos un emprendimiento llamado Parconier en el que buscamos crear un kit de robótica adaptado a la educación, que sea fácil de utilizar, modificable y reparable por los alumnos. Así nació Otus, un kit basado en Arduino, con piezas modulares que permiten armar cualquier tipo de robot, Open Source e impreso en 3D

![](../images/otus.jpg)
