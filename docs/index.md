## Hola!

Hola! Mi nombre es Pablo Brera y en esta página encontraras documentado mi recorrido a través de la Especialización en Fabricación Digital e Innovación Abierta (EFDIA) de la Universidad Tecnológica del Uruguay (UTEC) y el Fab Lab Barcelona

En la sección [About](https://pablo_brera.gitlab.io/pablo-brera/about/) podés encontrar información sobre quien soy y las razones que me motivaron a realizar el curso.

En la sección [Módulos](https://pablo_brera.gitlab.io/pablo-brera/modulos/MI01/) donde iré documentando los trabajos realizados en el curso y los aprendizajes adquiridos.

En la sección [Proyecto](https://pablo_brera.gitlab.io/pablo-brera/proyecto/laidea/) quedará documentado el proyecto final del curso el cual aplicara de forma combinada los conocimientos que haya adquirido durante esta experiencia.

Binevenidos/as!
